chat_Final.py does the man in the middle attack.

client_Final.py is our chatting program. It takes three arguments : a string "establish" or "connect", localhost, port

The program should run as follow : 
(1) First establish the connection : python client_Final.py establish localhost 8000
(2) Then, connect : python client_Final.py connect localhost 8000
(3) Then, start chat.