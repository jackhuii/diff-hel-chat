import socket
from AES_Encryption import aes_encryption as aes
from PublicKey import diffie_hellman as pb
import sys, select

# Pass arg as 'entropy' and then exponent.
# The second client only pass the exponent.

#client
addr = 'localhost'
port = 8000

entropy = 'entropy'
encrypt = aes()
encrypt.key, encrypt.hmac_key = encrypt.extract_keys(entropy)

instance = diffie_hellman()
#receive p and g from the server	
p = clientlist.recv(2048)
g = clientlist.recv(2048)
#generate key
bkey = instance.fast_mod(g,instance.exp,p) #g^exp mod p
client.send(bkey)
#receive key	
akey = clientlist.recv(4096)
secretkey = pow(g, akey, p)

#Establich connection to server.
client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
client.connect((addr, port))

lst = [sys.stdin, client]
while 1:
	read, write, err = select.select(lst, [], [])
	for e in read:
		if e == client:
			# read message.
			data = e.recv(2048)
			if data:
				print data, ' received!'
				print encrypt.dec(data, encrypt.key, encrypt.hmac_key)
		else:
			msg = sys.stdin.readline()
			msg = encrypt.enc(msg, encrypt.key, encrypt.hmac_key)
			client.send(msg)