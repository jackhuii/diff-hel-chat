from AES_Encryption import aes_encryption as aes
from PublicKey import diffie_hellman as pb
import select
import socket
import time
 
TIME_WAIT = 0.01
addr = 'localhost'
port = 8000
server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
server.bind((addr, port))
server.listen(2)
clients = []


print 'server waiting for the first client.'
# Accept the first client.
client, addr = server.accept()
print 'client addr : ', addr, 'connected!'
clients.append(client)

print 'server waiting for the second client.'
# Accept the second client.
client, addr = server.accept()
print 'client addr : ', addr, 'connected!'
clients.append(client)
 
running = True
pkey = False
gkey = False
entropyrec = False
instance = pb()
encrypt = aes()
encrypt.key, encrypt.hmac_key = 0
while running:
        readable, writable, err = select.select(clients, [], [])

        #If the first client send something alter and send it to client 2
        for c in readable:
			try:
				data = c.recv(2048)
				if len(buf) == 0:
					running = False
				
				#This is assuming that p and g is always the first thing sent through the server
				elif pkey == False: #MITM get p
					p = data
					pkey = True
					for i in clients:
						if i != c:
							i.send(data)
				elif gkey == False: #MITM get g
					g = data
					gkey = True
					for i in clients:
						if i != c:
							i.send(data)
				elif entropyrec == False: #MITM get entropy
					entropy = data
					encrypt.key, encrypt.hmac_key = encrypt.extract_keys(entropy)
					entropyrec = True
					for i in clients:
						if i != c:
							i.send(data)
							
				elif pkey == True:
					if gkey == True:			# Make our own key
							MITMkey = instance.fast_mod(g,instance.exp,p)
				
				elif len(buf) == 32:            #If exchanging keys send the MITM key
					for i in clients:
						if i != c:
							i.send(MITMkey)
				else:
					aes_enctyption.dec(buf, MITMkey,encrypt.hmac_key)    #decyrpt message sent by client
                
			except: 
				continue
server.close()            